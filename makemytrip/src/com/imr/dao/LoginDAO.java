package com.imr.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDAO {

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/makemytrip?useUnicode=yes&characterEncoding=UTF-8&useSSL=false", "root",
					"root");
			return con;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int checkLoginDetails(String user, String pass) {

		Connection con = getConnection();
		String qry = "SELECT EXISTS ( SELECT * FROM login_details WHERE username = ? AND password = ?)";
		try {
			PreparedStatement stmt = con.prepareStatement(qry);
			stmt.setString(1, user);
			stmt.setString(2, pass);

			ResultSet rs = stmt.executeQuery();
			int i = 0;
			while (rs.next()) {
				i = rs.getInt(1);
				System.out.println(i);
			}
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public String saveToDB(String f, String l, String u, String e, String pa, String ph) {

		Connection con = getConnection();
		try {
			PreparedStatement stmt = con.prepareStatement(
					"insert into login_details(firstname,lastname,username,email,password,phone) values (?,?,?,?,?,?)");
			stmt.setString(1, f);
			stmt.setString(2, l);
			stmt.setString(3, u);
			stmt.setString(4, e);
			stmt.setString(5, pa);
			stmt.setString(6, ph);

			stmt.executeUpdate();

			con.close();
			return "Successfully updated to the database";
		}catch (Exception o) {
			o.printStackTrace();
			return "Data is not inserted into the database";
		}
	}

}
