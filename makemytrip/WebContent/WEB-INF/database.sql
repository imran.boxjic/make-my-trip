create schema makemytrip


create table login_details (
id int not null auto_increment,
firstname varchar(100) not null,
lastname varchar(100) not null,
username varchar(100) not null,
email varchar(100) not null,
password varchar(100) not null,
phone varchar(10) not null,
primary key (id)
)